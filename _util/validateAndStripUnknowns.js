const Joi = require('joi')
const assert = require('node:assert')

/**
 * @template TSchema
 * @param {TSchema} value
 * @param {Array<AnySchema<unknown>>} schemata
 * @param {ValidationOptions} [options]
 * @return {AnySchema<TSchema>}
 */
function firstMatchingSchema(value, schemata, options) {
  assert(Array.isArray(schemata))
  assert(schemata.every(s => Joi.isSchema(s)))

  const match = schemata.find(s => {
    const { error } = s.validate(value, options)
    return !error
  })
  assert(match !== undefined)
  return match
}

/**
 * @template TSchema
 * @template {string} TPropertyName
 * @param {AnySchema<TSchema>} schema
 * @param {TPropertyName} propertyName
 * @return {AnySchema<TSchema[TPropertyName]>}
 */
function extractIfExists(schema, propertyName) {
  assert(Joi.isSchema(schema))
  assert(typeof propertyName === 'string')

  try {
    return schema.extract(propertyName)
  } catch (err) {
    assert(err.message.startsWith('Schema does not contain path'))
    return undefined
  }
}

/**
 * Precondition: `unStripped` adheres to the `schema`.
 *
 * @template TSchema
 * @param {TSchema} unStripped
 * @param {AnySchema<TSchema>} schema
 * @param {ValidationOptions} [options]
 * @return {TSchema}
 */
function strip(unStripped, schema, options) {
  assert(Joi.isSchema(schema))

  if (schema.type === 'array') {
    const result = []
    unStripped.forEach(item => {
      const value = strip(item, firstMatchingSchema(item, schema.$_terms.items, options), options)
      result.push(value)
    })
    return result
  }

  if (schema.type === 'object' && !schema.allowUnknown) {
    return Object.keys(unStripped).reduce((acc, propertyName) => {
      const propertySchema = extractIfExists(schema, propertyName)
      if (propertySchema) {
        acc[propertyName] = strip(unStripped[propertyName], propertySchema, options)
      } else {
        const keys = Object.keys(unStripped)
        keys.forEach(key => {
          const { error, value } = schema.validate({ [key]: unStripped[key] }, { abortEarly: false })
          if (!error) {
            acc[key] = value[key]
          }
        })
      }
      return acc
    }, {})
  }

  return unStripped
}

/**
 * _This function is introduced in response to issues discussed in
 * [fix: stripUnknown should honor local explicit .unknown(false)](https://github.com/hapijs/joi/pull/3037)._
 *
 * When we have a `schema` that, recursively, has objects with `.unknown(true)` set, to allow unknown properties in
 * validation, and we do
 *
 * ```javascript
 * …
 * const { value, error } = schema.validate(aValue, { stripUnknown: true, …})
 * if (error) {
 *   …
 *   throw …
 * }
 * useValue(value)
 * …
 * ```
 *
 * we expect the validation
 *
 * - to fail when there are unknown properties for objects or sub–objects that have `.unknown(false)` set, or not set at
 *   all (the default);
 * - to pass when there are unknown properties for objects or sub–objects that have `.unknown(true)` or `.unknown()`
 *   set; and
 * - if the validation passes, for `value` to have the unknown properties stripped.
 *
 * Since Joi `v17.13.2` this no longer works (see
 * [fix: stripUnknown should honor local explicit .unknown(false)](https://github.com/hapijs/joi/pull/3037)). We believe
 * that is wrong, but we the maintainers seem to have a different opinion, and do not seem to want to change back.
 *
 * This function merely strips unknown properties recursively, and is intended to be used as a workaround.
 *
 * Usage:
 *
 * ```javascript
 * …
 * const { value, error } = validateAndStripUnknowns(aValue, schema, {…})
 * if (error) {
 *   …
 *   throw …
 * }
 * useValue(value)
 * …
 * ```
 *
 * @template TSchema
 * @param {unknown} value
 * @param {AnySchema<TSchema>} schema
 * @param {ValidationOptions} [options]
 * @return {ValidationResult<TSchema>}
 */
function validateAndStripUnknowns(value, schema, options) {
  assert(Joi.isSchema(schema))

  /** @type {ValidationOptions | undefined} */
  const tweakedOptions = !options ? undefined : { ...options, stripUnknown: false }

  const stripped = strip(value, schema, tweakedOptions)
  const { value: unStripped, error } = schema.validate(stripped, tweakedOptions)

  if (error !== undefined) {
    return { value: unStripped, error }
  }

  return { value: stripped, error: undefined }
}

module.exports = { validateAndStripUnknowns, strip }
