/*
 * Copyright 2020 - 2022 PeopleWare n.v.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Joi = require('joi')

const should = require('should')
const testName = require('../../_util/_testName')
const { validateAndStripUnknowns } = require('../../_util/validateAndStripUnknowns')

const Deepest = Joi.object({
  deepestA: Joi.string().required()
}).unknown(true)

const Intermediate = Joi.object({
  intermediateA: Joi.number().optional(),
  deepest: Deepest.required()
}) // unknown false (default)

const AlmostTheTop = Joi.object({
  a: Joi.string().required(),
  b: Joi.number().optional(),
  intermediate: Intermediate.required()
}).unknown(true)

const Highest = Joi.array().items(AlmostTheTop.required(), Joi.boolean().required(), Joi.number().optional())

const passingDeepest1Stripped = { deepestA: 'the deepest 1' }
const passingDeepest1 = { ...passingDeepest1Stripped, unknownProperty1: 'you never saw me' }
const passingDeepest2 = { deepestA: 'the deepest 2' }

const passingIntermediate1Stripped = {
  intermediateA: 42,
  deepest: passingDeepest1Stripped
}
const passingIntermediate1 = {
  intermediateA: 42,
  deepest: passingDeepest1
}
const passingIntermediate2 = {
  deepest: passingDeepest2
}
const failingIntermediate = {
  intermediateA: 43,
  deepest: passingDeepest1,
  unknownNotAllowedProperty: 'I will not pass'
}

const passingAlmostTheTop1Stripped = {
  a: 'a 1',
  intermediate: passingIntermediate1Stripped
}
const passingAlmostTheTop1 = {
  a: 'a 1',
  intermediate: passingIntermediate1,
  unknownProperty2: 'you never saw me either'
}
const passingAlmostTheTop2Stripped = {
  a: 'a 2',
  b: 1 / 137,
  intermediate: passingIntermediate2
}
const passingAlmostTheTop2 = {
  ...passingAlmostTheTop2Stripped,
  unknownProperty3: "I'm not here"
}
const failingAlmostTheTop = {
  a: 'a 3',
  intermediate: failingIntermediate
}

const passingStripped = [passingAlmostTheTop1Stripped, passingAlmostTheTop2Stripped, true]
const passingHighest = [passingAlmostTheTop1, passingAlmostTheTop2, true]
const failingHighest = [passingAlmostTheTop1, failingAlmostTheTop, false]

describe(testName(module), function () {
  it('passes the schema', function () {
    Joi.attempt(passingHighest, Highest)
  })
  it('does not pass the schema', function () {
    Joi.attempt.bind(undefined, failingHighest, Highest).should.throw()
  })
  it('strips unknowns recursively when they are allowed', function () {
    const { value: stripped, error } = validateAndStripUnknowns(passingHighest, Highest)
    should(error).be.undefined()
    stripped.should.not.equal(passingHighest)
    stripped.should.deepEqual(passingStripped)
  })
  it("doesn't strips unknowns recursively when they are allowed", function () {
    const { value: stripped, error } = validateAndStripUnknowns(passingHighest, Highest)
    should(error).be.undefined()
    stripped.should.not.equal(passingHighest)
    stripped.should.deepEqual(passingStripped)
  })
  it('strips unknowns recursively when they are allowed with some options', function () {
    const { value: stripped, error } = validateAndStripUnknowns(passingHighest, Highest, {
      stripUnknown: true,
      convert: false
    })
    should(error).be.undefined()
    stripped.should.not.equal(passingHighest)
    stripped.should.deepEqual(passingStripped)
  })

  it('error should not be undefined', function () {
    const { error } = validateAndStripUnknowns({ test: 'test' },
      Joi.string().required())
    should(error).be.not.undefined()
  })

  it('should strip objects', function () {
    const itemSchema = Joi.object({
      key: Joi.string().required(),
      name: Joi.string().required()
    })
    const schema = Joi.object({
      intervals: Joi.array()
        .items(itemSchema)
        .description('test')
        .required()
    })

    const testDataStripped = {
      intervals: [
        {
          key: '5540',
          name: 'Persoonlijke bijdrage leven'
        },
        {
          key: '5540',
          name: 'Persoonlijke bijdrage leven'
        }
      ]
    }

    const testData = {
      intervals: [
        {
          key: '5540',
          name: 'Persoonlijke bijdrage leven'
        },
        {
          key: '5540',
          name: 'Persoonlijke bijdrage leven'
        }
      ],
      toDeleteProp: 'toDelete'
    }
    const { value: stripped, error } = validateAndStripUnknowns(testData, schema, {
      stripUnknown: true,
      convert: false
    })
    should(error).be.undefined()
    stripped.should.not.equal(testData)
    stripped.should.deepEqual(testDataStripped)
  })

  it('should copy key value schema', function () {
    const itemSchema = Joi.object({
      key: Joi.string().required(),
      name: Joi.string().required()
    })
    const schema = Joi.object().pattern(Joi.string().required(), itemSchema.required())

    const testData = {
      1: {
        key: '5540',
        name: 'Persoonlijke bijdrage leven'
      },
      2: {
        key: '5540',
        name: 'Persoonlijke bijdrage leven'
      }
    }

    const { value: stripped, error } = validateAndStripUnknowns(testData, schema, {
      stripUnknown: true,
      convert: false
    })
    should(error).be.undefined()
    stripped.should.deepEqual(testData)
  })
})
